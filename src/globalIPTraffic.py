import numpy as np
import matplotlib.pyplot as plt

def autolabel(rects):
    """Attach a text label above each bar in *rects*, displaying its height."""
    for rect in rects:
        height = rect.get_height()
        plt.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')

# Create the dataset:
height = [60, 72, 96, 122, 156, 201, 254, 319, 396]
bars = ('2014', '2015', '2016', '2017', '2018*', '2019*', '2020*', '2021*', '2022*')
y_pos = np.arange(len(bars))

#Bars
plt.ylim(0, 450)
plt.xlim(-1, 9)
plt.hlines([50, 100, 150, 200, 250, 300, 350, 400], -1, 9, linestyle="dotted", colors='#595959', alpha=.25)

# Create bars and add labels
rect = plt.bar(y_pos, height, color=['#00549f', '#00549f', '#00549f', '#00549f', '#00549f', '#00549f', '#9c9e9f', '#00549f', '#00549f'])
autolabel(rect)

# Create names on the x and y axis
plt.xticks(y_pos, bars)
plt.ylabel('Exabytes per Month')
plt.xlabel("*Prediction")

plt.savefig('src/vector graphics/globalIPTraffic.eps', format='eps')
plt.show()
