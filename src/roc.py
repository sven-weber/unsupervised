# libraries
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
 
# Data
cluster=pd.DataFrame({'detection': [0, 0.28, 0.47, 0.66, 0.93], 
                      'falsepositive': [0, 0.005, 0.01, 0.02, 0.1]})

c45 = pd.DataFrame({'detection': [0, 0.9, 0.93, 0.95], 
                   'falsepositive': [0, 0.0005, 0.001 , 0.01]})

# multiple line plot
plt.plot('falsepositive', 'detection', data=cluster, marker='o', markersize=10, linewidth=2, label="Cluster")
plt.plot('falsepositive', 'detection', data=c45, marker='x', linestyle='dashed', markersize=12, linewidth=2, label="c4.5*")

plt.ylim(0, 1.0)
plt.xlim(0, 0.1)
plt.xlabel('False Positive Rate')
plt.ylabel('Detection Rate')

# Save and Show
plt.legend()
plt.savefig('src/vector graphics/roc_curve.eps', format='eps')
plt.show()