# libraries
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.datasets.samples_generator import make_blobs

# Create a dataset:
x, group = make_blobs(n_samples=150, centers=1, cluster_std=0.2, random_state=0)

#Add example outlier
x = np.append(x, [[3, 2.7]], axis=0)
group = np.append(group, [1])

#Define color palete
colors = {0 : 'orange', 1: 'blue'}

#Create Dataframe
df = pd.DataFrame(dict(x=x[:, 0], y=x[:, 1], group=group))

# plot and display
plt.scatter( df['x'], df['y'], data=df, c=df['group'].apply(lambda x: colors[x]))
plt.xlim(0, 4)
plt.ylim(2, 5)
plt.savefig('src/vector graphics/unsupervised example.eps', format='eps')
plt.show()