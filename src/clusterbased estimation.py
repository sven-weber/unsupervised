# libraries
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.datasets.samples_generator import make_blobs

#
# ---- Functions ------
#
def main(mode):
    # Create a dataset:
    x, _ = make_blobs(n_samples=150, centers=1, cluster_std=0.2, random_state=0)

    #Perform fixed with clustering
    #Cluster size 10
    if (mode == 'fixedwithclustering1'): clusters = fixed_width_clustering(x, 0.25, 1)
    elif (mode == 'fixedwithclustering2'): clusters = fixed_width_clustering(x, 0.25, 2)
    else: clusters = fixed_width_clustering(x, 0.25, 0)

    #group and order clusters
    grouped = order_clusters(clusters)

    #Define color palete
    colors = { 0 : '#595959', 1 : 'orange', 2: 'blue', 3:'lightcoral', 4 :'green', 5:'gold', 6 : 'black', 7 : 'darkred', 8 : 'slategray', 9 : 'darkcyan', 10 : 'darkviolet'}

    #Create Dataframe
    df = pd.DataFrame(dict(x=x[:, 0], y=x[:, 1], clusters=clusters))

    if (mode == 'base'): 
        # plot and display
        plt.scatter(df['x'], df['y'], data=df, c=df['clusters'].apply(lambda x: '#595959'))
        
        plt.savefig(get_path('clusterbase.eps'), format='eps')
        plt.show()
    if (mode.startswith('distance')):
        # plot and display
        plt.scatter(df['x'], df['y'], data=df, c=df['clusters'].apply(lambda x: '#595959'))
        # plot lines to element
        if (mode == 'distance1'): element = 134
        else : element = 101
        for i in range(1, len(x)): 
            plt.plot([x[:, 0][element],x[:, 0][i]], [x[:, 1][element], x[:, 1][i]], 'r-')

        if (mode == 'distance1'): plt.savefig(get_path('clusterfirstdistance.eps'), format='eps')
        else: plt.savefig(get_path('clusterseconddistance.eps'), format='eps')
        plt.show()
    if  (mode.startswith('fixedwithclustering')):
        # plot and display
        plt.scatter(df['x'], df['y'], data=df, c=df['clusters'].apply(lambda x: colors[x]))
        #color clustercenter red        
        clustercenter = -1
        if(mode == 'fixedwithclustering1'): clustercenter = grouped[0][0]
        elif(mode == 'fixedwithclustering2'): clustercenter = grouped[1][0]
        if (clustercenter != -1): plt.plot(x[:, 0][clustercenter],x[:, 1][clustercenter], 'ro')
        if(mode == 'fixedwithclustering1'): plt.savefig(get_path('clusterfixedwithclustering1.eps'), format='eps')
        elif(mode == 'fixedwithclustering2'): plt.savefig(get_path('clusterfixedwithclustering2.eps'), format='eps')
        elif(mode == 'fixedwithclustering3'): plt.savefig(get_path('clusterfixedwithclustering3.eps'), format='eps')
        plt.show()
    if (mode.startswith('outliers')): 
        if(mode == 'outliers1'): threshold = 1
        else: threshold = 5
        # plot and display
        plt.scatter(df['x'], df['y'], data=df, c=df['clusters'].apply(lambda x: 'red' if(len(grouped[x-1]) <= threshold) else '#595959'))
        if(mode == 'outliers1'): plt.savefig(get_path('outliers1.eps'), format='eps')
        else: plt.savefig(get_path('outliers2.eps'), format='eps')
        plt.show() 
# Number sets the limitation of how much clusters should be created
# provide any value <= 0 to have no limitation
def fixed_width_clustering(points, width, number): 
    clusternumber = 1
    clusters = np.zeros(len(points), dtype=int)
    for i in range (0, len(points)): 
        if (clusters[i] != 0): continue
        for p in range(i, len(points)):
            if (clusters[p] == 0 and distance(points[p], points[i]) <= width): 
                clusters[p] = clusternumber
        clusternumber += 1
        if(number > 0 and clusternumber > number): break
    return clusters

def distance(v,w):
    return np.linalg.norm(v-w)

def order_clusters(clusters): 
    return pd.Series(range(len(clusters))).groupby(clusters, sort=False).apply(list).tolist()

def get_path(name): 
    return 'src/vector graphics/' + name
#
# ---- Execution ------
#
# Provide a 'mode' to the main method pointing out which plot you like
# 1. base
# 2. distance1
# 3. distance2
# 4. fixedwithclustering1
# 5. fixedwithclustering2
# 6. fixedwithclustering3
# 7. outliers1
# 8. outliers2
main('outliers2')