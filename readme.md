# Detecting Intrusions using Unsupervised Anomaly Detection

This repository contains the slides and source files for the 
Detecting Intrusions using Unsupervised Anomaly Detection
presentation. If you would like to perform the algorithms yourself please checkout the src folder.


# Packages required for installation

The source files provides in the repository are written in python. 
The following libraries need to be installed in order for the files work: 

Python 2
```
pip install matplotlib numpy pandas scikit-learn
```

Python 3
```
pip3 install matplotlib numpy pandas scikit-learn
```